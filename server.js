process.env.NODE_ENV = process.env.NODE_ENV || 'development'; 
const port = process.env.PORT || 3035;

// Main starting point of the application
const http = require('http');
const mongoose = require('./config/mongoose');
const express = require('./config/express');

// DB Setup
const db = mongoose(); 
const app = express();

// Server Setup
const server = http.createServer(app);
server.listen(port);

module.exports = app;

console.log('Server listening on:', port);

//test branch updateserver
