var jwt = require('jsonwebtoken');
var User = require('mongoose').model('User');
var passport = require('passport');
var fetch = require('node-fetch');
const { server } = require('../../../config/config');

exports.loginFacebook = (req, res, next) => {
  const { token } = req.body
  // Get the user's name using Facebook's Graph API
  const response = fetch(
    `https://graph.facebook.com/?fields=id,name,picture,address,email,gender,age_range,devices,birthday,first_name,last_name,link,languages,about,cover,hometown,location,relationship_status&access_token=${token}`)
    .then(res => {
      return res.json()
    })
    .then(json => {
      const { email, first_name, last_name } = json;
      // See if a user with the given email exists
      User.findOne({ email }, (err, existingUser) => {
        if (err) { return req.status(400).send(err) }

        // If a user with email does NOT exist, create and save user record
        const user = new User({ 
          email, 
          firstname: first_name, 
          lastname: last_name, 
          birthday: new Date(),
          password: 'Password!123',

        });

        if (existingUser) {
          return res.json({ success: true, token: tokenForUser(user) });
        }

        user.save((err) => {
          if (err) { return res.status(400).send(err) }
          // Repond to request indicating the user was created
          return res.json({ success: true, token: tokenForUser(user) });
        });
      });
    })

}


exports.saveOAuthUserProfile = function(req, profile, done) {
  User.findOne({
    provider: profile.provider,
    providerId: profile.providerId
  }, function(err, user) {
    if (err) {
      return done(err);
    } else {
      if (!user) {
        var possibleUsername = profile.username || ((profile.email) ? profile.email.split('@')[0] : '');

        User.findUniqueUsername(possibleUsername, null, function(availableUsername) {
          profile.username = availableUsername;

          user = new User(profile);

          user.save(function(err) {
            if (err) {
              var message = _this.getErrorMessage(err);

              req.flash('error', message);
              return res.redirect('/');
            }

            return done(err, user);
            res.json(req.user)
          });
        });
      } 
      else {
        return done(err, user);
      }
    }
  });
};

function tokenForUser(user) {
  return jwt.sign({ sub: user.id, email: user.email }, server.secret);
}

exports.signin = function (req, res, next) {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send({
      success: false,
      error: 'You must provide email and password'
    });
  }
  passport.authenticate('local', (err, user, info) => {
    if (err || !user) {
      return res.status(400).send(info);
    }
    // Remove sensitive data before login
    user.email = undefined;
    user.password = undefined;

    req.logIn(user, err => {
      if (err) {
        return res.status(400).send(err);
      }
      return res.json({ success: true, token: tokenForUser(req.body) });
    });
  })(req, res, next);
};

