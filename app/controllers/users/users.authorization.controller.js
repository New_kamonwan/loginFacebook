const jwt = require('jsonwebtoken');
const { server } = require('../../../config/config');

exports.requiresLogin = function (req, res, next) {
  const token = req.body.token || req.body.query || req.headers.authorization;
  if (token) {
    //verify token
    jwt.verify(token, server.secret, (err, decoded) => {
      if (err) {
        res.status(400).send({ success: false, error: 'Token invalid' }); //Token invalid
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    res.status(401).send({ success: false, error: 'User is not logged in' }); //No token provided
  }
};
