var passport = require('passport')
const Authentication = require('../controllers/users/users.authentication.controller')
const Authorization = require('../controllers/users/users.authorization.controller')
const passportService = require('../../config/passport')()
var path = require('path');

module.exports = function (app) {
  app.post('/signin', Authentication.signin)
  app.post('/authFacebook', Authentication.loginFacebook)
  
app.get('/', function(req, res){
  res.sendFile(path.resolve('views/login.html'));
});

app.get('/about', function(req, res){
  res.sendFile(path.resolve('views/home.html'));
});
  // Set up Facebook auth routes
  app.get('/oauth/facebook', passport.authenticate('facebook', {
    scope: [ 'email', 'public_profile'],
    session: false
  }));
  app.get(
    '/oauth/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: '/oauth/facebook'
    }), // Redirect user back to the mobile app using Linking with a custom protocol OAuthLogin
    (req, res) =>
      res.redirect('/about')
  )

}
