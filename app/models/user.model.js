const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const validate = require('mongoose-validator');
const Schema = mongoose.Schema;

// User E-mail Validator
const emailValidator = [
  validate({
      validator: 'matches',
      arguments: /^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/,
      message: 'Name must be at least 3 characters, max 40, no special characters or numbers, must have space in between name.'
  }),
  validate({
      validator: 'isLength',
      arguments: [3, 40],
      message: 'Email should be between {ARGS[0]} and {ARGS[1]} characters'
  })
];

const passwordValidator = [
  validate({
      validator: 'matches',
      //(?=.*?[\W])   special character
      arguments: /^(?=.*?[a-zA-Z])(?=.*?[\d]).{8,35}$/,
      message: 'Password needs to have at least one lower case, one uppercase, one number and must be at least 8 characters but no more than 35.'
  }),
  validate({
      validator: 'isLength',
      arguments: [8, 35],
      message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters'
  })
];

// Define our model
const userSchema = new Schema({
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  birthday: { type: Date, required: true },
  email: { type: String, unique: true, required: true, validate: emailValidator },
  password: { type: String, required: true, validate: passwordValidator },
  permission: { type: String, required: true, default: 'user' },
  created: { type: Date, default: Date.now }
});

// On Save Hook, encrypt password
// Before saving a model, run this function
userSchema.pre('save', function (next) {
  // get access to the user model
  const user = this;

  // generate a salt then run callback
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }

    // hash (encrypt) our password using the salt
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err); }

      // overwrite plain text password with encrypted password
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function (candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) { return callback(err); }

    callback(null, isMatch);
  });
}

mongoose.model('User', userSchema);
