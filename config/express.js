var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var compression = require('compression');
var cors = require('cors');
var passport = require('passport');

module.exports = function () {
  // App Setup
  const app = express();
  if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
  } else {
    app.use(compression());
  }

  app.use(cors());
  app.use(bodyParser.json({ type: '*/*' }));
  app.use(require('cookie-parser')());
  app.use(require('body-parser').urlencoded({ extended: true }));
  app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
  // Initialize Passport
  app.use(passport.initialize());
  app.use(passport.session());

  require('../app/routes/users.route')(app);

  return app;
};
