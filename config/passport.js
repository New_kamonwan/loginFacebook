const passport = require('passport');
const User = require('mongoose').model('User');

module.exports = function () {

  passport.serializeUser(function(user, done) {
    console.log('serializeUser: ' + user._id); //อยากส่งอะไรไปเก็บใน session
    done(null, user._id);
  });
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user){    //เอาของที่เก็บใน session มาใช้ต่อ
      console.log(user);
        if(!err) done(null, user);
        else done(err, null);
      });
  });

  // Create local strategy
  // require('./strategies/local')();
  require('./strategies/facebook')();
};
