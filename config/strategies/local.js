const passport = require('passport');
const User = require('mongoose').model('User');
const LocalStrategy = require('passport-local').Strategy;

module.exports = function () {
  // Create local strategy
  const localOptions = { usernameField: 'email' };

  passport.use(
    new LocalStrategy(localOptions, (email, password, done) => {
      User.findOne({ email }, (err, user) => {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, {
            success: false,
            error: 'Invalid username'
        });
        }

        // compare passwords - is `password` equal to user.password?
        user.comparePassword(password, (err, isMatch) => {
          if (err) {
            return done(err);
          }
          if (!isMatch) {
            return done(null, false, {
              success: false,
              error: 'Invalid password'
          });
          }

          return done(null, user);
        });
      });
    })
  );
};
