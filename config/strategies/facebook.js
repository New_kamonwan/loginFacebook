var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var url = require('url');
var users = require('../../app/controllers/users/users.authentication.controller');
var config = require('../config');
// const FacebookStrategy = require('passport-facebook').Strategy;
// const { facebook } = require('../config');

module.exports = function() {
    passport.use(new FacebookStrategy({
      clientID: config.facebook.clientID,
      clientSecret: config.facebook.clientSecret,
      callbackURL: config.facebook.callbackURL,
      profileFields: config.facebook.profileFields,     
      responseType: 'code',
      // auth_type: rerequest,
      passReqToCallback: true
    },

    function(req, accessToken, refreshToken, profile, done) {
      var providerData = profile._json;
      providerData.accessToken = accessToken;
      providerData.refreshToken = refreshToken;
  
      var providerUserProfile  = {
        firstName: profile.name.givenName,
        lastName: profile.name.familyName,
        fullName: profile.displayName,
        email: profile.emails[0].value,
        username: profile.username,
        provider: 'facebook',
        providerId: profile.id,
        providerData: providerData
      };
  
      users.saveOAuthUserProfile(req, providerUserProfile, done);
      //saveOAuthUserProfile
      console.log(providerUserProfile)
    }));
  };